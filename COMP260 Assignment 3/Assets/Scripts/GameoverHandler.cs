﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;



public class GameoverHandler : MonoBehaviour {
	public bool gameover = false;
	public Text gameoverText; 
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		if(gameover == true){
			if (Input.anyKeyDown){
				SceneManager.LoadScene (SceneManager.GetActiveScene ().buildIndex);
			}
		}
	
	}

	public void GameOverTrue(int prevscore){
		string newString = "GAME OVER \n YOU SCORED " + prevscore.ToString() + "\nPRESS ANY KEY TO CONTINUE";
		gameoverText.text = newString;
		gameover = true;
	}
}
