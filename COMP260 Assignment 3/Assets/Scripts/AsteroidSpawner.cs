﻿using UnityEngine;
using System.Collections;

public class AsteroidSpawner : MonoBehaviour {
	public GameObject asteroidPrefab; 
	public int numbAsteroids = 1; //this is only used for naming the prefab


	// Use this for initialization
	void Start () {

	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void SpawnNewAsteroid(){
		GameObject asteroid = Instantiate(asteroidPrefab);
		asteroid.transform.parent = transform;
		asteroid.gameObject.name = "Asteroid " + numbAsteroids;
		numbAsteroids = numbAsteroids + 1;


	}
}
