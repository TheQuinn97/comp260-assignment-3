﻿using UnityEngine;
using System.Collections;
[RequireComponent(typeof(Rigidbody2D))]
public class AsteroidHandler : MonoBehaviour {
	public float speed = 10f;
	public float width = 10f;
	public float height = 3f; 
	public float xMin = -5;
	public float yMin = -3;
	public float xTarget;
	public float yTarget;
	// Use this for initialization
	private Rigidbody2D asteroidRigidbody;

	void Start () {
		//rigidbody = GetComponent <Rigidbody2D> ();
		float x = xMin + Random.value * width;
		transform.position = new Vector2(x,yMin+height);
		xTarget = Random.value*width;
		yTarget = yMin;
		asteroidRigidbody = GetComponent <Rigidbody2D> ();
	}
	
	// Update is called once per frame
	void FixedUpdate () {
		Vector3 targetPosition = new Vector3 (xTarget,yTarget,1);
		Vector2 direction = targetPosition - transform.position;
		direction = direction.normalized;

		Vector2 velocity = direction * speed;
		asteroidRigidbody.AddForce (velocity * Time.deltaTime);


		if (asteroidRigidbody.position.y < yMin){
			float x = xMin + Random.value * width;
			transform.position = new Vector2(x,yMin);
			xTarget = Random.value*width;
			yTarget = yMin + height;	
			asteroidRigidbody.velocity = new Vector3(0,0,0);
			IncreaseSpeed();
		}
		if (asteroidRigidbody.position.y > yMin + height){
			float x = xMin + Random.value * width;
			transform.position = new Vector2(x,yMin+height);
			xTarget = Random.value*width;
			yTarget = yMin;	
			asteroidRigidbody.velocity = new Vector3(0,0,0);
			IncreaseSpeed();
	
		}
	}

	void IncreaseSpeed() {
		if (speed < 25){
			speed = speed + 1;
		}
	}


}
